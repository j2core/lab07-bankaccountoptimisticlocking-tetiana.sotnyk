package com.j2core.sts.dbworkers;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import com.j2core.sts.dbworker.DBWorker;
import com.j2core.sts.dbworker.OptimisticWorker;
import com.j2core.sts.dbworker.exception.DBWorkerException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.Collection;

/**
 * Created by sts on 7/10/16.
 */
public class OptimisticWorkerTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String TABLE_BALANCES = "CREATE TABLE balances ( id INT(11) NOT NULL AUTO_INCREMENT," +
            " company_name TEXT NOT NULL, sum INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static final String TABLE_ACCOUNTS_INFORMATION = "CREATE TABLE accounts_information (" +
            " id INT(11) NOT NULL AUTO_INCREMENT, account_name TEXT NOT NULL, company_name TEXT NOT NULL," +
            " movement_money INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static int counter = 10;


    @BeforeClass
    public void createTestDB(){

        String selectInsertAccount = "INSERT INTO accounts_information (account_name, company_name," +
                " movement_money, version) VALUES(?, 'Macys', 0.00, 1)";

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement();
        PreparedStatement preparedStatement = testConnection.prepareStatement(selectInsertAccount)){

            String sql = "CREATE DATABASE " + NAME_DB + " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
            testStatement.executeUpdate(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.executeUpdate(TABLE_BALANCES);
            testStatement.executeUpdate(TABLE_ACCOUNTS_INFORMATION);

            testStatement.execute("INSERT INTO balances (company_name, sum, version) VALUES('Macys', 100000.00, 1)");

            while (counter > 0){

                String accountName = "account" + counter;
                preparedStatement.setString(1, accountName);

                preparedStatement.execute();

                counter--;
            }

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry!");
        }

    }

    @BeforeGroups(groups = "dbWorker")
    public OptimisticWorker createDBWorker(){

        return new OptimisticWorker();

    }


    @BeforeGroups(groups = "dbWorker")
    public ConnectionPool createConnectionPool() {

        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";
        return new ConnectionPool("St", 30, 100, 250, fullDBUrl, USER, PASS);
    }


    @Test(groups = "dbWorker")
    public void testGetAccount() throws Exception {

        String accountName = "account10";
        ConnectionPool connectionPool = createConnectionPool();

        OptimisticWorker worker = createDBWorker();

        Account account = new Account(1, accountName, "Macys", 0.00, 1);

        Account testAccount = worker.getAccount(connectionPool, accountName);

        Assert.assertTrue(account.equals(testAccount));

    }


    @Test(groups = "dbWorker")
    public void testChangeAccountMovementMoney() throws Exception {

        OptimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();
        String account_name = "account1";

        worker.changeAccountsMovementMoney(connectionPool.getConnection(), account_name, 1, -10.00);

        Account account = worker.getAccount(connectionPool, account_name);

        Assert.assertEquals(account.getMovementMoney(), -10.00, 0.001);

    }


    @Test(groups = "dbWorker")
    public void testGetCompanyBalance() throws Exception {

        String companyName = "Macys";
        ConnectionPool connectionPool = createConnectionPool();

        OptimisticWorker worker = createDBWorker();
        CompanyBalance balance = new CompanyBalance(1, companyName, 100000.00, 1);

        CompanyBalance testBalance = worker.getCompanyBalance(connectionPool, companyName);

        Assert.assertTrue(balance.equals(testBalance));

    }


    @Test(groups = "dbWorker")
    public void testChangeSumBalance() throws Exception {

        OptimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();
        String company_name = "Macys";

        if (!worker.changeSumBalance(connectionPool.getConnection(), company_name, 1, -10)){
            throw new Exception();
        }

        CompanyBalance balance = worker.getCompanyBalance(connectionPool, company_name);

        Assert.assertEquals(balance.getBalance(), 99990.00, 0.001);

    }


    @Test(groups = "dbWorker")
    public void testGetCompanyAccounts() throws SQLException {

        int amountAccounts = 10;

        OptimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();

        Collection<Account> collection = worker.getCompanyAccounts(connectionPool, "Macys", amountAccounts);

        Assert.assertTrue(collection.size() == amountAccounts);

    }


    @Test
    public void testChangeBalance() throws Exception {

        String companyName = "Macys";

        OptimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();

        worker.changeBalance(connectionPool, companyName, "account2", -10.00);

        CompanyBalance companyBalance = worker.getCompanyBalance(connectionPool, companyName);

        connectionPool.removeShutdownHook();

        Assert.assertEquals(companyBalance.getBalance(), 99980.00, 0.001);

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";

        try (Connection conn = DriverManager.getConnection(fullDBUrl, USER, PASS); Statement stmt = conn.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
