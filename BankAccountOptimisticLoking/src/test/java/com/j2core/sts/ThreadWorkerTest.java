package com.j2core.sts;

import com.j2core.sts.coordinator.Coordinator;
import com.j2core.sts.dbworker.DBWorker;
import com.j2core.sts.dbworker.OptimisticWorker;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.sql.*;

import static java.lang.Thread.sleep;

/**
 * Created by sts on 7/10/16.
 */
public class ThreadWorkerTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String TABLE_BALANCES = "CREATE TABLE balances ( id INT(11) NOT NULL AUTO_INCREMENT," +
            " company_name TEXT NOT NULL, sum INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static final String TABLE_ACCOUNTS_INFORMATION = "CREATE TABLE accounts_information (" +
            " id INT(11) NOT NULL AUTO_INCREMENT, account_name TEXT NOT NULL, company_name TEXT NOT NULL," +
            " movement_money INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static final int amountAccounts = 10;

    @BeforeClass
    public void createTestDB(){

        String selectInsertAccount = "INSERT INTO accounts_information (account_name, company_name," +
                " movement_money, version) VALUES(?, 'Macys', 0.00, 0)";

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement();
        PreparedStatement statement = testConnection.prepareStatement(selectInsertAccount)){

            String sql = "CREATE DATABASE " + NAME_DB; //" DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.executeUpdate(TABLE_BALANCES);
            testStatement.executeUpdate(TABLE_ACCOUNTS_INFORMATION);

            testStatement.execute("SET innodb_lock_wait_timeout = 250");

            testStatement.execute("INSERT INTO balances (company_name, sum, version) VALUES('Macys', 100000.00, 0)");

            int createdAmountAccounts = 1;

            while (createdAmountAccounts <= amountAccounts){

                String accountName = "account" + createdAmountAccounts;
                statement.setString(1, accountName);

                statement.execute();

                createdAmountAccounts++;
            }

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry!");
        }
    }


    @Test(groups = "Optimistic")
    public void testThreadWorkerOptimistic() throws SQLException {

        int amountAccountsThreads = 10;
        int amountThreadTransactions = 10;
        double balanceDecreasing = -10.00;
        String companyName = "Macys";
        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";
        ConnectionPool connectionPool = new ConnectionPool("St", 75, 250, 5000, fullDBUrl, USER, PASS);
        DBWorker dbWorker = new OptimisticWorker();

        new Thread(new Coordinator(connectionPool, amountAccounts, amountAccountsThreads, amountThreadTransactions, balanceDecreasing, dbWorker, companyName)).run();

        CompanyBalance balance = dbWorker.getCompanyBalance(connectionPool, companyName);


        connectionPool.release();

        Assert.assertEquals(balance.getBalance(), 90000.00, 0.001);

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";

        try (Connection connection = DriverManager.getConnection(fullDBUrl, USER, PASS); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
