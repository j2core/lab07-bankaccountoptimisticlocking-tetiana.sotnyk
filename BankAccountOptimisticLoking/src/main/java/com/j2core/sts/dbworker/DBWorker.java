package com.j2core.sts.dbworker;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.Collection;

/**
 * Created by sts on 7/10/16.
 */

/**
 * Interface for take Company's balance and accounts information, change company's balance in Data Bases
 */
public interface DBWorker {

 /**
  * Method take Company balance from Data Base
  *
  * @param connectionPool ConnectionPool for work with DB
  * @param companyName    Name company's which information need take
  * @return company's information
  * @throws SQLException if have problem with work DB
  */
 CompanyBalance getCompanyBalance(ConnectionPool connectionPool, String companyName) throws SQLException;


 /**
  * Method changed company's balance in DB
  *
  * @param connectionPool Connection pool for work with DB
  * @param companyName  company's name which balance changing
  * @param accountName  account's name which changing company's balance
  * @param sumChanging  sum changing company's balance
  * @return balance's change successfully or not
  * @throws Exception if balance's sum less sum changing
  */
 boolean changeBalance(ConnectionPool connectionPool, String companyName, String accountName, double sumChanging) throws Exception;


 /**
  * Method take all company's accounts
  *
  * @param connectionPool Connection pool for work with DB
  * @param companyName  company's name which accounts need take
  * @param amountAccounts amount company's accounts which need take
  * @return Collection with company's accounts
  * @throws SQLException if have problem with work DB
  */
 Collection<Account> getCompanyAccounts(ConnectionPool connectionPool, String companyName, int amountAccounts) throws SQLException;

}
