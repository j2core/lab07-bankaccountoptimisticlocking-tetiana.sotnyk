package com.j2core.sts.dbworker;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import com.j2core.sts.dbworker.exception.DBWorkerException;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;


/**
 * Created by sts on 7/7/16.
 */

/**
 * Class for work with company's balance and account's movement with optimistic lock DB
 */
public class OptimisticWorker implements DBWorker{

    private static final double INACCURACY = 0.001; // inaccuracy for equals value balances ana movements
    public static final Logger LOGGER = Logger.getLogger(OptimisticWorker.class); // class for save logs information

    /**
     * Constructor for OptimisticWorker's class
     */
    public OptimisticWorker() {

    }


    @Override
    public boolean changeBalance(ConnectionPool connectionPool, String nameCompany, String accountName, double sumChanging) throws Exception {

        boolean result = false;
        CompanyBalance companyBalance = getCompanyBalance(connectionPool, nameCompany);
        Account account = getAccount(connectionPool, accountName);

        try (Connection connection = connectionPool.getConnection()) {

            connection.setAutoCommit(false);

            if ((companyBalance.getBalance() - sumChanging) > INACCURACY) {

                if (changeSumBalance(connection, nameCompany, companyBalance.getVersion(), sumChanging)) {

                    if (changeAccountsMovementMoney(connection, accountName, account.getVersion(), sumChanging)) {
                        result = true;

                    } else throw new SQLException("I can't change account information. Sorry.");

                } else throw new SQLException("I can't change balance. Sorry.");

            } else throw new Exception("Not have money");

            connection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return result;
    }


    @Override
    public CompanyBalance getCompanyBalance(ConnectionPool connectionPool, String companyName) throws SQLException {

        CompanyBalance companyBalance = null;
        String selectCompanyBalance = "SELECT * FROM balances WHERE company_name = ?";

        try (Connection connection = connectionPool.getConnection(); PreparedStatement statement = connection.prepareStatement(selectCompanyBalance)) {

            connection.setAutoCommit(false);

            statement.setString(1, companyName);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    companyBalance = new CompanyBalance(resultSet.getInt("id"), resultSet.getString("company_name"), resultSet.getDouble("sum"), resultSet.getInt("version"));
                }
            }

            connection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return companyBalance;

    }


    /**
     * Method changed sum balance's value
     *
     * @param connection    connection for work with DB
     * @param company_name  company's name which balance need chang
     * @param version       data's version
     * @param sumChanging   sum changing company's balance
     * @return change balance successfully or not
     * @throws SQLException if have problem with work DB
     */
    public boolean changeSumBalance(Connection connection, String company_name, int version, double sumChanging) throws SQLException {

        boolean result = false;
        String selectUpdateBalance = "UPDATE balances SET sum = sum + ?, version = version + 1 WHERE company_name = ? AND version = ?";

        try (PreparedStatement updateStatement = connection.prepareStatement(selectUpdateBalance)) {

            updateStatement.setDouble(1, sumChanging);
            updateStatement.setString(2, company_name);
            updateStatement.setInt(3, version);

            if (0 < updateStatement.executeUpdate()) {
                result = true;
            }

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return result;
    }


    @Override
    public Collection<Account> getCompanyAccounts(ConnectionPool connectionPool, String companyName, int amountAccounts) throws SQLException {

        Collection<Account> accounts = new LinkedList<Account>();
        String selectCompanyAccounts = "SELECT * FROM accounts_information WHERE company_name = ? LIMIT ?";

        try (Connection connection = connectionPool.getConnection(); PreparedStatement statement = connection.prepareStatement(selectCompanyAccounts)) {

            connection.setAutoCommit(false);

            statement.setString(1, companyName);
            statement.setInt(2, amountAccounts);

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    accounts.add(new Account(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getDouble(4), resultSet.getInt(5)));

                }
            }

            connection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return accounts;
    }


    /**
     * Method take company's account
     *
     * @param connectionPool  connection pool for work with DB
     * @param accountName  account's name which need take
     * @return  company's account
     * @throws SQLException if have problem with work DB
     */
    public Account getAccount(ConnectionPool connectionPool, String accountName) throws SQLException {

        Account account = null;
        String selectAccountsInformation = "SELECT * FROM accounts_information WHERE account_name = ?";

        try (Connection connection = connectionPool.getConnection(); PreparedStatement statement = connection.prepareStatement(selectAccountsInformation)) {
            statement.setString(1, accountName);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    account = new Account(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getDouble(4), resultSet.getInt(5));

                }
            }

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return account;
    }


    /**
     * Method changed value account's movement
     *
     * @param connection connection for work with DB
     * @param account_name account's name which movement need change
     * @param version      data's version
     * @param sumChanging  sum changing company's balance
     * @return  change movement successfully or not
     * @throws SQLException if have problem with work DB
     */
    public boolean changeAccountsMovementMoney(Connection connection, String account_name, int version, double sumChanging) throws SQLException {

        boolean result = false;
        String selectUpdateStatement = "UPDATE accounts_information SET movement_money = movement_money + ?, version = version + 1 WHERE account_name = ? AND version = ?";

        try (PreparedStatement updateStatement = connection.prepareStatement(selectUpdateStatement)){

            updateStatement.setDouble(1, sumChanging);
            updateStatement.setString(2, account_name);
            updateStatement.setInt(3, version);

                if (0 < updateStatement.executeUpdate()) {
                    result = true;
                }

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return result;

    }

}
