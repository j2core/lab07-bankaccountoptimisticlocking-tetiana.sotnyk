package com.j2core.sts.coordinator;

import com.j2core.sts.dbworker.DBWorker;
import com.j2core.sts.dbworker.exception.DBWorkerException;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

/**
 * Created by sts on 7/7/16.
 */

/**
 * Class perform a task with change company's balance and account's movement money
 */
public class ThreadWorker implements Runnable {

    public static final Logger LOGGER = Logger.getLogger(ThreadWorker.class);
    private final String nameThread;
    private final DBWorker dbWorker;
    private final String companyName;
    private final String accountName;
    private int transactionCounter;
    private double deltaChangeSum;
    private final Object sync;
    private final ThreadsCounter threadsCounter;
    private final ConnectionPool connectionPool;


    /**
     * Constructor ThreadWorker's class
     *
     * @param nameThread        thread's name
     * @param dbWorker          class for work with DB
     * @param amountTransaction amount transaction for work
     * @param companyName       company's name
     * @param accountName       account's name
     * @param deltaChangeSum    sum for changing company's balance and account's movement money
     * @param sync              class for synchronization with all threads
     * @param threadsCounter    class for count amount threads
     * @param connectionPool    connection pool for work with DB
     */
    public ThreadWorker(String nameThread, DBWorker dbWorker, int amountTransaction, String companyName, String accountName, double deltaChangeSum, Object sync, ThreadsCounter threadsCounter, ConnectionPool connectionPool) {

        this.nameThread = nameThread;
        this.dbWorker = dbWorker;
        this.transactionCounter = amountTransaction;
        this.companyName = companyName;
        this.accountName = accountName;
        this.deltaChangeSum = deltaChangeSum;
        this.sync = sync;
        this.threadsCounter = threadsCounter;
        this.connectionPool = connectionPool;
    }

    @Override
    public void run() {

        LOGGER.info("Started thread - " + nameThread);

        // wait created all threads
        synchronized (sync) {
            try {
                sync.wait();
            } catch (InterruptedException e) {
                LOGGER.error(e);
            }

            LOGGER.info(" Thread woke up");
            while (transactionCounter > 0) {

                try {

                    // changing balance
                    if (dbWorker.changeBalance(connectionPool, companyName, accountName, deltaChangeSum)) {
                        transactionCounter--;
                        LOGGER.info("Changed balance");
                    } else {
                        throw new Exception("not good change balance (ThreadWorker)");
                    }

                } catch (DBWorkerException ex) {
                    LOGGER.error("Sorry!" + ex);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            threadsCounter.takeThread();

            // if this lost thread - wake up coordinator
            if (threadsCounter.getAmountTreads() < 1) {
                synchronized (sync) {
                    sync.notifyAll();
                    LOGGER.info(" Coordinator - wake up!");
                }
            }

            LOGGER.info("Stopped thread - ( " + threadsCounter.getAmountTreads() + " )" + nameThread);

        }
    }
}
