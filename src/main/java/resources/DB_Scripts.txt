
CREATE TABLE accounts_information (
                                    id INT(11) NOT NULL AUTO_INCREMENT,
                                    account_name TEXT NOT NULL,
                                    company_name TEXT NOT NULL,
                                    movement_money INT(11) NOT NULL,
                                    version INT(11) NOT NULL,
                                    PRIMARY KEY (id)
                                    );

CREATE TABLE balances (
                                    id INT(11) NOT NULL AUTO_INCREMENT,
                                    company_name TEXT NOT NULL,
                                    sum INT(11) NOT NULL,
                                    version INT(11) NOT NULL,
                                    PRIMARY KEY (id)
                                    );