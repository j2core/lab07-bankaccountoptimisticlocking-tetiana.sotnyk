package com.j2core.sts.coordinator;

/**
 * Created by sts on 7/13/16.
 */

/**
 * Class for save amount threads
 */
public class ThreadsCounter {

    private int amountTreads;

    /**
     * Constructor ThreadsCounter's class
     *
     * @param amountTreads amount threads
     */
    public ThreadsCounter(int amountTreads) {

        this.amountTreads = amountTreads;
    }

    /**
     * Method deduction thread from counter
     */
    public synchronized void   takeThread(){

        amountTreads = amountTreads - 1;

    }

    public synchronized int getAmountTreads(){
        return amountTreads;
    }
}
