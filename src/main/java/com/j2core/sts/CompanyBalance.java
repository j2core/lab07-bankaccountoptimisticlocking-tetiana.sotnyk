package com.j2core.sts;

/**
 * Created by sts on 7/7/16.
 */

/**
 * Class for save company's name, id in DB, version data, and value sun company's balance
 */
public class CompanyBalance {

    private final double balance;
    private final int version;
    private final String companyName;
    private final int id;


    /**
     * Constructor for CompanyBalance's class
     *
     * @param id     id company in DB
     * @param companyName  company's name
     * @param balance  sum company's balance
     * @param version  version data
     */
    public CompanyBalance(int id, String companyName, double balance, int version) {
        this.balance = balance;
        this.version = version;
        this.companyName = companyName;
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public int getVersion() {
        return version;
    }

    public String getCompanyName() {
        return companyName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        CompanyBalance that = (CompanyBalance) obj;

        if (Double.compare(that.balance, balance) != 0) return false;
        if (version != that.version) return false;
        if (id != that.id) return false;
        return companyName.equals(that.companyName);

    }

    @Override
    public int hashCode() {
        int result;
        result = 31 + version;
        result = 31 * result + companyName.hashCode();
        result = 31 * result + id;
        return result;
    }
}


